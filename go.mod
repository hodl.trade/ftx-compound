module hodl.trade/ftx-compound

go 1.16

require (
	github.com/getsentry/sentry-go v0.10.0
	github.com/joho/godotenv v1.3.0
	hodl.trade/exchanges v0.0.0-20210315030407-935a81aceb8f
)
