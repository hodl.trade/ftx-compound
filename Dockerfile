FROM debian:10-slim

RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates

ENTRYPOINT ["/usr/local/bin/ftx-compound"]

COPY ftx-compound /usr/local/bin/ftx-compound
