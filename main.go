package main

import (
	"context"
	"fmt"
	"log"
	"math"
	"os"
	"time"

	_ "github.com/joho/godotenv/autoload"

	"github.com/getsentry/sentry-go"

	"hodl.trade/exchanges/ftx"
	"hodl.trade/exchanges/httputil"
)

func main() {
	var err error

	if dsn := os.Getenv("SENTRY_DSN"); dsn != "" {
		err := sentry.Init(sentry.ClientOptions{
			Dsn:     dsn,
			Release: "ftx-compound",
			BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
				return event
			},
		})
		if err != nil {
			log.Fatalf("sentry.Init: %s", err)
		}

		defer sentry.Flush(2 * time.Second)
		defer sentry.Recover()
	}

	ctx := context.Background()
	ctx = ftx.WithSignature(ctx, &ftx.Credentials{
		ApiKey:     os.Getenv("FTX_API_KEY"),
		ApiSecret:  os.Getenv("FTX_API_SECRET"),
		SubAccount: os.Getenv("FTX_SUBACCOUNT"),
	})

	once := len(os.Args) > 1 && os.Args[1] == "--once"
	for {
		// compounding more than once a day has insignificant return
		//
		// year	month	week	day	hourly
		// APY	1	12	52	365	8760
		// 20.0000%	20.0000%	1.6667%	0.3846%	0.0548%	0.0023%
		// 1000.00	200.00	219.39	220.93	221.34	221.40
		// 10000.00	2000.00	2193.91	2209.34	2213.36	2214.00
		//
		// compound every 12h (+5 min) to have some robustness in case of outage etc.
		if !once {
			n := time.Now()
			<-time.After(n.Truncate(12 * time.Hour).Add(12*time.Hour + 5*time.Minute).Sub(n))
		}

		var info []struct {
			Coin     string  `json:"coin"`
			Lendable float64 `json:"lendable"`
			Locked   float64 `json:"locked"`
			MinRate  float64 `json:"minRate"`
			Offered  float64 `json:"offered"`
		}
		_, err = httputil.GetJSON(ctx, ftx.API_URL, "spot_margin/lending_info", nil, &info, &ftx.ResponseWrap{})
		if err != nil {
			panic(err)
		}

		for _, i := range info {
			if d := i.Lendable - i.Locked; d > 0.01 {
				fmt.Printf("Reinvesting accrued interest %s %.02f min. %s APY ...", i.Coin, d, annualPercentageYield(i.MinRate, time.Hour))

				size := math.Trunc(i.Lendable*100) / 100
				fmt.Printf(" %.3f ... ", size-i.Locked)

				_, err = httputil.PostJSON(ctx, ftx.API_URL, "spot_margin/offers", nil, map[string]interface{}{
					"coin": i.Coin,
					"size": size,
					"rate": i.MinRate,
				}, nil, &ftx.ResponseWrap{})

				if err != nil {
					fmt.Println("💣")

					fmt.Println(err)
					fmt.Printf("Lendable:%.8f\n", i.Lendable)

					panic(err)
				}
				fmt.Println("💰")
			}
		}

		if once {
			break
		}
	}
}

func annualPercentageYield(v float64, interval time.Duration) string {
	if interval == time.Hour {
		v *= 24
	}
	return fmt.Sprintf("%.02f%%", v*365*100)
}
